# pacman-human-input

Adds some human friendly versions of the Arch Linux pacman commands. Each one has sudo built in where necessary so you can just run them.

## Commands

### Install a package:
`pacman-install <package_name>`

### Remove a package:
`pacman-remove <package_name>`

### Search for a package:
`pacman-search <text>`

### Update the package list:
`pacman-update`

### Upgrade all installed packages:
`pacman-upgrade-all`

### Upgrade a single package:
`pacman-upgrade <package_name>`